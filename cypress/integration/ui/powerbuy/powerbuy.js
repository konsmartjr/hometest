import tv_validation from '../../../fixtures/powerbuy/data/datatest/hometest/product_validation.json'
import filter_url from '../../../fixtures/powerbuy/data/label/hometest/product_url.json'
import added_product from '../../../fixtures/powerbuy/data/label/hometest/product_checking.json'
const powerbuy = require('../../../fixtures/powerbuy/locator/powerbuy')

describe('Powerbuy Shopping', () => {
    beforeEach(() => {
        cy.viewport(1024, 768)
    })
    context('Verify Product Filter and Product Information', () => {
        it('Validate result of items in the cart match with the items that user added', () => {
            cy.visit(filter_url.filter_tv_55_inch.url)
            cy.get(powerbuy.obj.TV_55_Inch, { timeout: 20000 })
                .should('be.visible')
                .contains(added_product.tv_55_inch.name)
                .click({ force: true })
            cy.get(powerbuy.obj.Add_Cart_55_Inch, { timeout: 20000 })
                .should('be.visible')
                .click({ force: true })
            cy.visit(filter_url.filter_tv_32_inch.url)
            cy.get(powerbuy.obj.TV_32_Inch, { timeout: 20000 })
                .should('be.visible')
                .contains(added_product.tv_32_inch.name)
                .click({ force: true })
            cy.get(powerbuy.obj.Add_Cart_32_Inch, { timeout: 20000 })
                .scrollIntoView()
                .should('be.visible')
                .click({ force: true })
            cy.get(powerbuy.obj.Mini_Cart, { timeout: 20000 })
                .then(el => {
                    el[0].click({ force: true });
                    return el;
                })
            cy.get(powerbuy.obj.Check_Cart_32_Inch, { timeout: 20000 })
                .parent()
                .should('contain', tv_validation['32_inch_name'])
            cy.get(powerbuy.obj.Check_Cart_55_Inch, { timeout: 20000 })
                .parent()
                .should('contain', tv_validation['55_inch_name'])
        })
    })
})