// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
// import './utilities/cache'
import './commands'
import 'cypress-mochawesome-reporter/register';
Cypress.on('uncaught:exception', (err, runnable) => {
    // returning false here prevents Cypress from
  // failing the test
    return false
    })
// Alternatively you can use CommonJS syntax:
// require('./commands')

let GenidCard = () => Math.floor(Math.random() * 10000000000000);
let idCard = `${GenidCard()}`
Cypress.env('IdCard' , idCard)

let GenProposalNo = () => Math.floor(Math.random() * 10000000000000);
let ProposalNo = `TEMP_${GenProposalNo()}`
Cypress.env('ProposalNo' , ProposalNo)

let GenclientAppID = () => Math.floor(Math.random() * 10000000000000);
let clientAppID = `TEMP_${GenclientAppID()}`
Cypress.env('clientAppID' , clientAppID)

// const xlsx = require("node-xlsx").default;
// const fs = require("fs");
// const path = require("path");

// module.exports = (on, config) => {
//   // `on` is used to hook into various events Cypress emits
//   on("task", {
//     parseXlsx({ filePath }) {
//       return new Promise((resolve, reject) => {
//         try {
//           const jsonData = xlsx.parse(fs.readFileSync(filePath));
//           resolve(jsonData);
//         } catch (e) {
//           reject(e);
//         }
//       });
//     }
//   });
// };