const common = require('../../fixtures/powerbuy/locator/powerbuy')

import '@testing-library/cypress/add-commands';
import { configure } from '@testing-library/cypress'

configure({ testIdAttribute: 'data-test-id' })

Cypress.Commands.add('waitForService', (method, endpoint, alias, timeout, waittime) => {
    if (!waittime) {
        waittime = 100
    }

    cy.route(method, endpoint)
        .as(alias)
        .wait(`@${alias}`, { timeout: timeout })
        .wait(waittime)
})

Cypress.Commands.add('waitLoading', (timeout) => {

    if (!timeout) {
        timeout = 10000
    }

    cy.findByTestId(common.obj.loading_icon).should('exist')
    cy.findByTestId(common.obj.loading_icon, { timeout: timeout }).should('not.exist')
    cy.wait(2000)
})

Cypress.Commands.add('waitLoadingPopup', (text, timeout) => {

    if (!timeout) {
        timeout = 10000
    }

    cy.findByText(text).should('exist')
    cy.findByText(text, { timeout: timeout }).should('not.exist')
    cy.wait(2000)
})

Cypress.Commands.add('waitForTextVisible', (text, timeout) => {
    if (!timeout) {
        timeout = 10000
    }

    cy.contains(text, { timeout: timeout }).should('be.visible')
})

Cypress.Commands.add('checkExisting', (locator) => {
    cy.findByTestId(locator).should('exist')
})

Cypress.Commands.add('checkNotExisting', (locator) => {
    cy.findByTestId(locator).should('not.exist')
})

Cypress.Commands.add('checkExistingWithText', (locator, text) => {
    cy.findByTestId(locator).contains(text).should('exist')
})

Cypress.Commands.add('checkExistingWithTextByLabel', (label, text) => {
    label = createRegexForInput(label)

    cy.get('label').contains(label).parent().parent().contains(text).should('exist')
})

Cypress.Commands.add('checkExistingWithTextByLabelWithHeader', (topic, label, text) => {
    label = createRegexForInput(label)

    cy.get('div').parent().parent().contains(topic).get('label').contains(label).parent().parent().parent().contains(text).should('exist')
})

Cypress.Commands.add('checkNotExistingWithTextByLabel', (label, text) => {
    label = createRegexForInput(label)

    cy.get('label').contains(label).parent().parent().contains(text).should('not.exist')
})

Cypress.Commands.add('checkNotExistingWithText', (locator, text) => {
    cy.findByTestId(locator, { timeout: 5000 }).contains(text).should('not.exist')
})

Cypress.Commands.add('checkTextExisting', (text) => {
    cy.contains(text).should('exist')
})

Cypress.Commands.add('checkTextExistingWithHeader', (topic, text) => {
    cy.get('div').contains(topic).parent().parent().contains(text).should('exist')
})

Cypress.Commands.add('checkHeadlineTextExisting', (text) => {
    cy.get('h2').contains(text).should('exist')
})

Cypress.Commands.add('checkTextNotExisting', (text) => {
    cy.contains(text).should('not.exist')
})

Cypress.Commands.add('checkTextNotExistingWithHeader', (topic, text) => {
    cy.get('div').contains(topic).parent().parent().contains(text).should('not.exist')
})

Cypress.Commands.add('inputText', (locator, text) => {
    cy.findByTestId(locator).find('input')
        .clear().type(text)
})

Cypress.Commands.add('inputTextSpecialChar', (locator, text) => {
    cy.get(locator).find('textarea')
        .click().clear().type(text, { parseSpecialCharSequences: false })
})

Cypress.Commands.add('useDevice', (device) => {

    Cypress.on('window:before:load', win => {
        Object.defineProperty(win.navigator, 'userAgent', {
            value:
                'Mozilla/5.0 (iPad; CPU OS 13_6 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 (com.oracle.ofsc/20.8.0.63/ios)'
        })
    })
    cy.viewport('ipad-2')
})

Cypress.Commands.add('getParamFromGET', (url) => {
    var parameters = {}
    var i

    var url_spt = url.split('?')
    let endpiont = url_spt[1]
    var endpiont_spt = endpiont.split('&')

    for (i = 0; i < endpiont_spt.length; i++) {
        let param = endpiont_spt[i].split('=')
        parameters[param[0]] = param[1]
    }
    return parameters

})

Cypress.Commands.add('setSystemDate', (val) => {
    const now = new Date(val.year, val.month - 1, val.day).getTime()
    cy.clock(now)
})
Cypress.Commands.add('mockServiceResponse', (name, method, url, status, response) => {
    cy.route({
        method: method,
        url: url,
        status: status,
        response: response
    }).as(name)
})

export const convertMonthByNum = (month_no) => {

    var month
    switch (month_no) {
        case '01':
            month = 'ม.ค.'
            break
        case '02':
            month = 'ก.พ.'
            break
        case '03':
            month = 'มี.ค.'
            break
        case '04':
            month = 'เม.ย.'
            break
        case '05':
            month = 'พ.ค.'
            break
        case '06':
            month = 'มิ.ย.'
            break
        case '07':
            month = 'ก.ค.'
            break
        case '08':
            month = 'ส.ค.'
            break
        case '09':
            month = 'ก.ย.'
            break
        case '10':
            month = 'ต.ค.'
            break
        case '11':
            month = 'พ.ย.'
            break
        case '12':
            month = 'ธ.ค.'
            break
    }

    return month
}

export const convertMonthByNumEN = (month_no) => {

    var month
    switch (month_no) {
        case '01':
            month = 'Jan'
            break
        case '02':
            month = 'Feb'
            break
        case '03':
            month = 'Mar'
            break
        case '04':
            month = 'Apr'
            break
        case '05':
            month = 'May'
            break
        case '06':
            month = 'Jun'
            break
        case '07':
            month = 'Jul'
            break
        case '08':
            month = 'Aug'
            break
        case '09':
            month = 'Sep'
            break
        case '10':
            month = 'Oct'
            break
        case '11':
            month = 'Nov'
            break
        case '12':
            month = 'Dec'
            break
    }

    return month
}

//----------------------------------------//
export function createRegexForInput(text) {
    text = text.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
    text = new RegExp("^" + text + " " + "|" + "^" + text + "$")

    return text
}

export function createIDcardFormat(id_no) {
    const id_no_arr = id_no.split("")
    id_no_arr.splice(1, 0, " ")
    id_no_arr.splice(6, 0, " ")
    id_no_arr.splice(12, 0, " ")
    id_no_arr.splice(14, 0, " ")
    return id_no_arr.join('')
}

export function createTelephoneFormat(phone_no) {
    const phone_no_arr = phone_no.split("")
    phone_no_arr.splice(3, 0, " ")
    phone_no_arr.splice(7, 0, " ")
    return phone_no_arr.join('')
}

function createCurrencyFormat(number) {
    return parseFloat(number).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
}

function createNameTHFormat(title, name, lastname) {
    return title + " " + name + " " + lastname
}

function createNameENFormat(title, name, lastname) {
    if (name == '' || lastname == '') {
        return "-"
    } else {
        return `${title} ${name} ${lastname}`
    }
}

function createFullDateFormat(datetime) {
    if (datetime.includes('/')) {
        var datetime_arr = datetime.split("/")
    } else {
        var datetime_arr = datetime.split("-")
    }

    datetime_arr[0] = datetime_arr[0].replace(/(^|-)0+/g, "$1")
    datetime_arr[1] = convertMonthByNum(datetime_arr[1])
    var result = datetime_arr.filter((_, i, datetime_arr) => i > datetime_arr.length - 5)

    return result.join(' ')
}

function getBirthYear(val) {
    var val_arr = val.split("")
    var result = val_arr.filter((_, i, val_arr) => i > val_arr.length - 5)
    return result.join('')
}

export function changeFormatData(type, text) {

    switch (type) {
        case 'id':
            text = createIDcardFormat(text)
            break
        case 'phone':
            text = createTelephoneFormat(text)
            break
        case 'name_th':
            text = createNameTHFormat(text[0], text[1], text[2])
            break
        case 'name_en':
            text = createNameENFormat(text[0], text[1], text[2])
            break
        case 'age':
            var current_year = Cypress.env('systemRefDate').year + 543
            var birth_year = getBirthYear(text)

            text = (current_year - birth_year)
            break
        // แก้ไขทีหลัง มันยากก
        case 'currency':
            text = createCurrencyFormat(text)
            break
        case 'date':
            text = createFullDateFormat(text)
            break
    }

    return text
}

//กรอกลง textbox (type คือ optional 'id','phone','name_th','name_en','age','date', 'none')
Cypress.Commands.add('inputDataByLabel', (label, text, type) => {
    label = createRegexForInput(label)

    var validate_text = text
    
    if (type) {
        var validate_text = changeFormatData(type, text)
    }

    // แก้ไขทีหลัง มันยากกกก
    if (type == 'date' || type == 'none') {
        cy.get('label').contains(label).parent().parent().siblings().findByRole('textbox')
            .clear({ force: true }).type(text).blur()
    } else {
        cy.get('label').contains(label).parent().parent().siblings().findByRole('textbox')
            .clear({ force: true }).type(text).blur()
            .should('have.value', validate_text)
    }
})

Cypress.Commands.add('inputDataByLabelWithHeader', (topic, label, text, type) => {
    label = createRegexForInput(label)

    var validate_text = text

    if (type) {
        var validate_text = changeFormatData(type, text)
    }

    // แก้ไขทีหลัง มันยากกกก
    if (type == 'date' || type == 'none') {
        cy.get('div').contains(topic).parent().parent().find('label').contains(label).parent().parent().siblings().findByRole('textbox')
            .clear().type(text).blur()
    } else {
        cy.get('div').contains(topic).parent().parent().find('label').contains(label).parent().parent().siblings().findByRole('textbox')
            .clear().type(text).blur()
            .should('have.value', validate_text)
    }
})

Cypress.Commands.add('inputDataByLabelNoValidate', (label, text) => {
    label = createRegexForInput(label)
    
    cy.get('label').contains(label).parent().parent().siblings().findByRole('textbox')
        .clear().type(text).blur()
})

Cypress.Commands.add('inputDataByLabelNoValidateWithHeader', (topic, label, text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(topic).parent().parent().find('label').contains(label).parent().parent().siblings().findByRole('textbox')
        .clear().type(text).blur()
})

Cypress.Commands.add('inputDataByLabelNoValidateWithHeader2', (topic, label, text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(topic).parent().siblings().find('label').contains(label).parent().parent().siblings().findByRole('textbox')
        .clear().type(text).blur()
})

//เลือกจาก dropdownlist
Cypress.Commands.add('selectDataByLabel', (label, item_text, synctime) => {
    label = createRegexForInput(label)

    if (synctime) {
        synctime = synctime
    } else {
        synctime = 100
    }

    cy.get('div').contains(label).parent().parent().siblings().findByRole('listbox').click()
        .wait(synctime)
        .then(($el) => {
            cy.get($el).find('span').contains(`${item_text}`).click()
            cy.get($el).should('contain', item_text)
        })
})
Cypress.Commands.add('selectDropdownByID', (item_text, test_id, synctime) => {
    
    
    if (synctime) {
        synctime = synctime
    } else {
        synctime = 100
    }

    cy.findByTestId('insured-input-'+ test_id).click()
        .wait(synctime)
        .then(($el) => {
            cy.get($el).find('span').contains(`${item_text}`).click()
            cy.get($el).should('contain', item_text)
        })
})
Cypress.Commands.add('selectDataByLabelWithHeader', (topic, label, item_text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(topic).parent().parent().find('label').contains(label).parent().parent().siblings().findByRole('listbox').click()
        .then(($el) => {
            cy.get($el).find('span').contains(`${item_text}`).click()
            cy.get($el).should('contain', item_text)
        })
})

Cypress.Commands.add('selectDropdownSearchDataByLabel', (label, item_text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(label).parent().parent().siblings().findByRole('combobox').click()
        .then(($el) => {
            cy.get($el).type(item_text)
            cy.get($el).find('span').contains(`${item_text}`).click()
            cy.get($el).should('contain', item_text)
        })
})

Cypress.Commands.add('selectDropdownSearchData', (locator, item_text) => {
    
    cy.get('div').findByTestId(locator).click()
        .then(($el) => {
            cy.get($el).type(item_text)
            cy.get($el).find('span').contains(`${item_text}`).click()
            cy.get($el).should('contain', item_text)
        })
})

Cypress.Commands.add('selectDropdownSearchDataByLabelWithHeader', (topic, label, item_text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(topic).parent().parent().find('label').contains(label).parent().parent().siblings().findByRole('combobox').click()
        .then(($el) => {
            cy.get($el).type(item_text)
            cy.get($el).find('span').contains(`${item_text}`).click()
            cy.get($el).should('contain', item_text)
        })
})

Cypress.Commands.add('typeDataComboByLabel', (label, item_text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(label).parent().parent().siblings().findByRole('combobox').click()
        .then(($el) => {
            cy.get($el).type(item_text)
        })
})

//เลือกจาก radio button
Cypress.Commands.add('chooseDataByLabel', (label, radio_text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(label).parent().parent().siblings().findByText(radio_text).click()
        .parent().find('input').should('be.checked')
})

Cypress.Commands.add('chooseDataByLabelWithHeader', (topic, label, radio_text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(topic).parent().parent().find('div').contains(label).parent().parent().siblings().findByText(radio_text).click()
        .parent().parent().find('input')
})

Cypress.Commands.add('selectCheckboxByText', (text) => {
    text = createRegexForInput(text)

    cy.get('div').contains(text).siblings('div').click()
        .parent().find('input').should('be.checked')
})

Cypress.Commands.add('deselectCheckboxByText', (text) => {
    text = createRegexForInput(text)

    cy.get('div').contains(text).siblings('div').click()
        .parent().find('input').should('not.be.checked')
})

//ติ๊กใน checkbox
Cypress.Commands.add('checkDataByLabel', (label, checkbox_text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(label).siblings().findByText(checkbox_text).click()
    // verify later ยังไม่มี component ให้เล่น
})

//กดปุ่ม button
Cypress.Commands.add('clickButtonByLabel', (label) => {
    label = createRegexForInput(label)

    cy.findAllByRole('button').contains(label).click()
})

Cypress.Commands.add('clickButtonByLocator', (locator) => {

    cy.findAllByTestId(locator).click()
})

Cypress.Commands.add('clickButtonByLabelWithHeader', (topic, label) => {
    label = createRegexForInput(label)

    cy.get('div').contains(topic).parent().parent().find('div').contains(label).click({ force: true })
})

Cypress.Commands.add('clickButtonByImgWithHeader', (topic) => {

    cy.get('div').contains(topic).parent().parent().find('img').click()
})

Cypress.Commands.add('clickItemByLabel', (label) => {
    label = createRegexForInput(label)

    cy.contains(label).click()
})

Cypress.Commands.add('clickItemByLabelWithHeader', (topic, label) => {
    label = createRegexForInput(label)

    cy.get('div').contains(topic).parent().parent().contains(label).click({ force: true })
})

Cypress.Commands.add('clickButton', (locator) => {
    cy.findByTestId(locator).click()
})
//---------validate contains ------//

//ตรวจสอบว่า textbox มีข้อมูลที่กรอก (type เป็น optional = 'id','phone','name_th','name_en','age','date')
Cypress.Commands.add('checkInputedDataByLabel', (label, text, type) => {
    label = createRegexForInput(label)

    if (type) {
        text = changeFormatData(type, text)
    }

    cy.get('label').contains(label).parent().parent().siblings().findByRole('textbox')
        .should('have.value', text)
})

Cypress.Commands.add('checkSelectDataByLabelWithHeader', (topic, label, text, type) => {
    label = createRegexForInput(label)

    if (type) {
        text = changeFormatData(type, text)
    }

    cy.get('div').contains(topic).parent().parent().find('label').contains(label).parent().parent().get('div').contains(text)
})

Cypress.Commands.add('checkInputedAddressDataByLabelWithHeader', (topic, label, text, type) => {
    label = createRegexForInput(label)

    if (type) {
        text = changeFormatData(type, text)
    }

    cy.get('div').contains(topic).parent().parent().find('label').get('textarea').contains(text)
})

//ตรวจสอบว่า radio button เลือกค่านี้ตามที่กด
Cypress.Commands.add('checkChoosedDataByLabel', (label, radio_text) => {
    label = createRegexForInput(label)

    cy.get('label').contains(label).parent().parent().siblings().findByText(radio_text).parent().find('input').should('be.checked')
})

Cypress.Commands.add('checkChoosedDataByLabelWithHeader', (topic, label, radio_text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(topic).parent().parent().find('div').contains(label).parent().parent().siblings().findByText(radio_text).parent().parent().find('input').should('be.checked')
})


Cypress.Commands.add('checkChoosedDataAndValidateDisableByLabel', (label, val) => {
    label = createRegexForInput(label)

    cy.get('label').contains(label).parent().parent().findByText(val).parent().find('input').should('be.checked')
    cy.get('label').contains(label).parent().parent().findByText(val).should('have.attr', 'disabled')
})

//---------validate NOT contains ------//

//ตรวจสอบว่า textbox ไม่มีข้อมูลที่กรอก (type เป็น optional = 'id','phone')
Cypress.Commands.add('checkNoInputedDataByLabel', (label, text, type) => {
    label = createRegexForInput(label)

    if (type) {
        text = changeFormatData(type, text)
    }

    cy.get('label').contains(label).parent().parent().siblings().findByRole('textbox')
        .should('not.have.value', text)
})

//ตรวจสอบว่า radio button ไม่เลือกค่านี้
Cypress.Commands.add('checkNoChoosedDataByLabel', (label, radio_text) => {
    label = createRegexForInput(label)

    cy.get('label').contains(label).parent().parent().siblings().findByText(radio_text).parent().find('input').should('not.be.checked')
})

Cypress.Commands.add('checkNoSelectedDataByLabel', (label, text, type) => {
    label = createRegexForInput(label)

    if (type) {
        text = changeFormatData(type, text)
    }
    cy.get('div').contains(label).parent().parent().siblings().findByRole('listbox').should('not.contain', text)
})

Cypress.Commands.add('checkSelectedDataByLabel', (label, text, type) => {
    label = createRegexForInput(label)

    if (type) {
        text = changeFormatData(type, text)
    }
    cy.get('div').contains(label).parent().parent().siblings().findByRole('listbox').should('contain', text)
})

//----------------------- verify message -----------//

//ตรวจสอบว่า มี warning message
Cypress.Commands.add('verifyWarningMessageByLabel', (label, msg) => {
    label = createRegexForInput(label)

    msg = createRegexForInput(msg)

    cy.get('label').contains(label).parent().parent().siblings().contains(msg).should('exist')
})


Cypress.Commands.add('verifyWarningMessageByLabelWithHeader', (topic, label, msg) => {
    label = createRegexForInput(label)

    msg = createRegexForInput(msg)

    cy.get('div').contains(topic).parent().parent().find('label').contains(label).parent().parent().siblings().contains(msg).should('exist')
})

//ตรวจสอบว่า ไม่มี warning message
Cypress.Commands.add('verifyNoWarningMessageByLabel', (label, msg) => {
    label = createRegexForInput(label)

    msg = createRegexForInput(msg)

    cy.get('label').contains(label).parent().parent().siblings().contains(msg).should('not.exist')
})

Cypress.Commands.add('verifyNoWarningMessageByText', (text, msg) => {
    text = createRegexForInput(text)

    msg = createRegexForInput(msg)

    cy.get('div').contains(text).parent().parent().contains(msg).should('not.exist')
})

// ตรวจสอบ ข้อมูล ที่อยู่ใต้ label
Cypress.Commands.add('verifyDataByLabel', (label, text, type) => {
    label = createRegexForInput(label)

    if (type) {
        text = changeFormatData(type, text)
    }

    cy.get('div').contains(label).parent().parent().siblings().should('contain', text)
})

Cypress.Commands.add('verifyNoDataByLabel', (label, text, type) => {
    label = createRegexForInput(label)

    if (type) {
        text = changeFormatData(type, text)
    }

    cy.get('div').contains(label).parent().parent().siblings().should('not.contain', text)
})

Cypress.Commands.add('verifyPlaceholderByLabel', (label, msg) => {
    label = createRegexForInput(label)
    cy.get('label').contains(label).parent().parent().parent().find('input').should('have.attr', 'placeholder', msg)

})

//----------- Check Component ---------//
Cypress.Commands.add('verifyComponentTextboxByLabel', (label, component_type, check_text) => {
    label = createRegexForInput(label)

    var locator = getComponentType(component_type)
    if (check_text) {
        cy.get('div').contains(label).parent().parent().siblings().find(locator).should('have.value', check_text)
    } else {
        cy.get('div').contains(label).parent().parent().siblings().find(locator).should('exist')
    }
})

Cypress.Commands.add('verifyComponentDropdownByLabel', (label, component_type, check_text) => {
    label = createRegexForInput(label)

    var locator = getComponentType(component_type)
    if (check_text) {
        cy.get('div').contains(label).parent().parent().siblings().find(locator).contains(check_text).should('exist')
    } else {
        cy.get('div').contains(label).parent().parent().siblings().find(locator).should('exist')
    }
})

Cypress.Commands.add('verifyComponentRadioByLabel', (label, component_type, check_text) => {
    label = createRegexForInput(label)

    var locator = getComponentType(component_type)

    if (check_text) {
        if (check_text == "true") {
            cy.get('div').contains(label).parent().parent().find(locator).parent().parent().find('input').should('be.checked')
        } else {
            cy.get('div').contains(label).parent().parent().find(locator).parent().parent().find('input').should('not.be.checked')
        }
    } else {
        cy.get('div').contains(label).parent().parent().siblings().find(locator).should('exist')
    }
})

Cypress.Commands.add('verifyComponentCheckboxByLabel', (label, component_type, check_text) => {
    label = createRegexForInput(label)
    var locator = getComponentType(component_type)
    if (check_text) {
        if (check_text == "true") {
            cy.get('div').contains(label).parent().parent().find(locator).parent().parent().find('input').should('be.checked')
        } else {
            cy.get('div').contains(label).parent().parent().find(locator).parent().parent().find('input').should('not.be.checked')
        }
    } else {
        cy.get('div').contains(label).parent().parent().siblings().find(locator).should('exist')
    }
})

export function getComponentType(component_type) {

    switch (component_type) {
        case 'textbox':
            component_type = 'input'
            break
        case 'dropdown':
            component_type = '[role="alert"]'
            break
        case 'radio':
            component_type = '[type="radio"]'
            break
        case 'checkbox':
            component_type = 'input'
            break
        case 'button':
            component_type = 'button'
            break
        case 'slidebar':
            locator_type = 'slidebar'
            break
    }
    return component_type
}

Cypress.Commands.add('verifySlidebarValue', (text, check_text) => {
    text = createRegexForInput(text)

    cy.get('div').contains(text).parent().parent().siblings().children().children().children().should('contain', check_text)

})

Cypress.Commands.add('verifyChoosedDataAndValidateIsDisable', (label, isDisabled) => {
    label = createRegexForInput(label)

    if (isDisabled) {
        cy.get('div').contains(label).siblings().find('img').parent().should('have.attr', 'disabled')
    } else {
        cy.get('div').contains(label).siblings().find('img').parent().should('not.have.attr', 'disabled')
    }
})

Cypress.Commands.add('clearInputDataByLabelNoValidate', (label) => {
    label = createRegexForInput(label)

    cy.get('label').contains(label).parent().parent().siblings().findByRole('textbox')
        .clear()
})

Cypress.Commands.add('verifyComponentNotPassValidation', (label, component_type) => {
    label = createRegexForInput(label)

    var locator = getComponentType(component_type)

    if (component_type == 'textbox' || component_type == 'dropdown') {
        cy.get('div').contains(label).parent().parent().siblings().find(locator).parent()
            .should('have.css', 'border', Cypress.env('err_color'))

    } else {
        cy.get('div').contains(label).parent().parent().siblings().find(locator).siblings()
            .should('have.css', 'border', Cypress.env('err_color'))
    }
})

Cypress.Commands.add('inputDataByLabelWithHeaderFHC', (topic, label, text, type) => {
    label = createRegexForInput(label)

    if (type) {
        var validate_text = changeFormatData(type, text)
    }

    cy.get('div').contains(topic).parent().parent().children().find('label').contains(label).parent().parent().parent().findByRole('textbox')
        .clear({ force: true }).type(text).blur()
        .should('have.value', validate_text)
})

Cypress.Commands.add('inputDataByLabelNoValidateWithHeaderFHC', (topic, label, text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(topic).parent().parent().children().find('label').contains(label).parent().parent().parent().findByRole('textbox')
        .clear({ force: true }).type(text).blur()
})

Cypress.Commands.add('checkInputedDataByLabelWithHeaderFHC', (topic, label, text, type) => {
    label = createRegexForInput(label)

    if (type) {
        text = changeFormatData(type, text)
    }

    cy.get('div').contains(topic).parent().parent().children().find('label').contains(label).parent().parent().parent().findByRole('textbox')
        .should('have.value', text)
})

Cypress.Commands.add('verifyDisabledDropdownListByLabel', (label, isDisabled) => {
    label = createRegexForInput(label)
    if (isDisabled) {
        cy.get('div').contains(label).siblings('img').should('have.attr', 'disabled')
    } else {
        cy.get('div').contains(label).siblings('img').should('not.have.attr', 'disabled')
    }
})

Cypress.Commands.add('verifyDisabledTextboxByLabel', (label) => {
    label = createRegexForInput(label)
    cy.get('label').contains(label).parent().parent().siblings().findByRole('textbox')
        .parent().should('have.attr', 'disabled')
})

Cypress.Commands.add('verifyDisabledDropDownListByLabel', (label) => {
    label = createRegexForInput(label)
    cy.get('div').contains(label).parent().parent().siblings().findByRole('listbox')
        .parent().should('have.attr', 'disabled')
})

Cypress.Commands.add('verifyDisabledDropDownSearchByLabel', (label) => {
    label = createRegexForInput(label)
    cy.get('div').contains(label).parent().parent().siblings().findByRole('combobox')
        .parent().should('have.attr', 'disabled')
})

Cypress.Commands.add('verifyDisabledCheckboxByText', (text) => {
    text = createRegexForInput(text)

    cy.get('div').contains(text).siblings('div')
        .should('have.attr', 'disabled')
})

Cypress.Commands.add('verifyDisabledRadioByLabel', (label, radio_text) => {
    label = createRegexForInput(label)

    cy.get('div').contains(label).parent().parent().siblings().findByText(radio_text)
        .parent().find('input').should('be.disable')
})

Cypress.Commands.add('verifyDisableButtonByLabel', (label) => {
    label = createRegexForInput(label)

    cy.get('div').contains(label).should('have.attr', 'disabled')
})

Cypress.Commands.add('verifyDisableButtonByLabelWithHeader', (topic, label) => {
    label = createRegexForInput(label)

    cy.get('div').contains(topic).parent().siblings().find('div').contains(label).should('be.disabled')
})

Cypress.Commands.add('verifyComponentIsDisableByLabel', (label) => {
    cy.get('div').contains(label).should('be.disabled')
})

Cypress.Commands.add('verifyDisabledDropDownListByLabel', (label) => {
    label = createRegexForInput(label)

    cy.get('div').contains(label).parent().parent().siblings().findByRole('listbox')
        .parent().should('have.attr', 'disabled')
})

Cypress.Commands.add('verifyDisabledTextboxByLabel', (label) => {
    label = createRegexForInput(label)

    cy.get('label').contains(label).parent().parent().siblings().findByRole('textbox')
        .parent().should('have.attr', 'disabled')
})

Cypress.Commands.add('clearInputDataByLabelNoValidate', (label) => {
    label = createRegexForInput(label)

    cy.get('label').contains(label).parent().parent().siblings().findByRole('textbox')
        .clear().blur()
})

Cypress.Commands.add('verifyDisabledDropDownSearchByLabel', (label) => {
    label = createRegexForInput(label)

    cy.get('div').contains(label).parent().parent().siblings().findByRole('combobox')
        .parent().should('have.attr', 'disabled')
})

Cypress.Commands.add('verifyDisabledCheckboxByText', (text) => {
    text = createRegexForInput(text)

    cy.get('div').contains(text).siblings('div')
        .should('have.attr', 'disabled')
})

Cypress.Commands.add('verifyDisabledRadioByLabel', (label, radio_text) => {
    label = createRegexForInput(label)
 
    cy.get('div').contains(label).parent().parent().siblings().findByText(radio_text)
        .parent().find('input').should('be.disable')
})

Cypress.Commands.add('moveChildrenSliderBar', (locator) => {
    var count = 1
    var i
    var rightarrow = '{rightarrow}'
    var arrowsum_text
    for (i = 0; i < count; i++) {
        arrowsum_text += rightarrow;
    }
    cy.findByTestId(locator).type(arrowsum_text, { force: true })
})

Cypress.Commands.add('scrollToCheckVisible', (locator, label) => {
    cy.findByTestId(locator).siblings().find('div').contains(label).should('exist')
    .then(el => {
       el[0].scrollIntoView();
         return el;
       })
    .should('be.visible')
 })
Cypress.Commands.add('selectCheckbox', (locator) => {

    cy.findByTestId(locator).click()
      .parent().find('input').should('be.checked')
})

Cypress.Commands.add('deselectCheckbox', (locator) => {

    cy.findByTestId(locator).click()
      .parent().find('input').should('not.be.checked')
})

Cypress.Commands.add('moveSliderbarToRight', (locator, count) => {
    var i
    var rightarrow = '{rightarrow}'
    var arrowsum_text = ""
    for (i = 0; i < count; i++) {
        arrowsum_text += rightarrow;
    }
    cy.findByTestId(locator).type(arrowsum_text, { force: true })
  })
  
  Cypress.Commands.add('moveSliderbarToLeft', (locator, count) => {
    var i
    var leftarrow = '{leftarrow}'
    var arrowsum_text = ""
    for (i = 0; i < count; i++) {
        arrowsum_text += leftarrow;
    }
    cy.findByTestId(locator).type(arrowsum_text, { force: true })
  })
